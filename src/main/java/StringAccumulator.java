import java.util.Arrays;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import static org.apache.commons.lang.StringUtils.isBlank;
import static org.apache.commons.lang.StringUtils.isEmpty;


public class StringAccumulator {

    public static final String EMPTY_STRING = "";
    public static final String COMMA = ",";
    public static final String NEW_LINE = "\n";
    public static final String ESCAPED_NEWLINE = "\\n";
    public static final String PIPE = "|";
    private static final String ESCAPED_PIPE = "\\|";
    public static final String DOUBLE_PIPE = "||";
    public static final String ESCAPED_BACKSLASH = "\\";
    public static final String regExSpecialChars = "<([{\\^-=$!|]})?*+.>";
    public static final String regExSpecialCharsRE = regExSpecialChars.replaceAll(".", "\\\\$0");
    public static final Pattern reCharsREP = Pattern.compile("[" + regExSpecialCharsRE + "]");


    public static String escapeRegexCharacters(String s) {
        Matcher m = reCharsREP.matcher(s);
        return m.replaceAll("\\\\$0");
    }

    ;

    public int add(String s) {
        int returnValue = 0;
        if (isEmpty(s)) return 0;
       /* 2. // Allow the add method to handle an unknown amount of numbers.
        if (numbers.length > 2) {
            throw new IllegalArgumentException("Max 2 nos are allowed");
        }*/
        String numbersString = s;
        String delimiterString = COMMA;
        // Escape all regex characters
        s = escapeRegexCharacters(s);

        // we dont need to escape NEW_LINE & PIPE as that is one of the identifier for delimiters
        s = s.replace(ESCAPED_NEWLINE, NEW_LINE);
        s = s.replace(ESCAPED_PIPE, PIPE);


        if (s.startsWith("//") && (s.contains(NEW_LINE))) { // identifier condition for demiliter specifier
            // Extracting numbers, Numbers will occur after new line
            numbersString = s.substring(s.indexOf(NEW_LINE) + 1, s.length());

            String validDelimiters = s
                    .substring(2, s.indexOf(NEW_LINE))            // Extracting delimiters,
                    .replace(ESCAPED_BACKSLASH, PIPE + ESCAPED_BACKSLASH);  // To handle multiple length delimiters
            delimiterString = delimiterString + (isBlank(validDelimiters) ? validDelimiters : PIPE + validDelimiters);
            // Multiple length delimited "non-escaping" can lead to joined pipes
            delimiterString = delimiterString.replace(DOUBLE_PIPE, PIPE);
        }
        numbersString = numbersString
                .replace(NEW_LINE, COMMA)
                .replace(ESCAPED_BACKSLASH, EMPTY_STRING);  // un-escape regex characters

        if (numbersString.endsWith(COMMA)) throw new IllegalArgumentException(", cannot be at end of the numbers");

        String[] numbers = numbersString.split(delimiterString);

        int[] negativeNos = Arrays.stream(numbers)
                .filter(number -> !isEmpty(number))
                .mapToInt(s2 -> Integer.parseInt(s2))
                .filter(n -> n < 0)
                .toArray();

        if (negativeNos != null && negativeNos.length > 0) {
            throw new IllegalArgumentException(String.format("negatives not allowed  : %s", Arrays.toString(negativeNos)));
        }

        returnValue = Arrays.stream(numbers)
                .filter(number -> !isEmpty(number))
                .mapToInt(s2 -> Integer.parseInt(s2))
                .filter(i -> i < 1000)
                .sum();
        return returnValue;
    }
}
