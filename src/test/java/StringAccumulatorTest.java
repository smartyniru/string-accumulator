import org.junit.Assert;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;

import static org.junit.Assert.*;

public class StringAccumulatorTest {


    private StringAccumulator stringAccumulator;

    @Rule
    public ExpectedException expectedEx = ExpectedException.none();

    @Before
    public void setUp() {
        stringAccumulator = new StringAccumulator();
    }

    /**
     * There is no need to check for invalid inputs
     */
    @Test(expected = Exception.class)
    public final void willNotHandleErroneousInput() {
        assertEquals(6, stringAccumulator.add("abcad"));
    }

    /**
     * 1. Create a simple string calculator with a method int add(String numbers)
     *      a. The method can take 0, 1 or 2 numbers and will return their sum (for an empty string  it will return 0) for example "" or "1” or "1,2”
     */
    @Test
    public final void expect0ForEmptyString() {
        assertEquals(0, stringAccumulator.add(""));
    }


    /**
     * 1. Create a simple string calculator with a method int add(String numbers)
     *      b. Start with the simplest test case of an empty string and move to 1 and 2 numbers.
     */
    @Test
    public final void expectCorrectValueForSingleNoStrings() {
        assertEquals(1, stringAccumulator.add("1"));
        assertEquals(3, stringAccumulator.add("1,2"));
    }

//    2. Allow the add method to handle an unknown amount of numbers.
     /** 1. Create a simple string calculator with a method int add(String numbers)
     *      a. The method can take 0, 1 or 2 numbers and will return their sum (for an empty string  it will return 0) for example "” or "1” or "1,2”
     */
//    @Test(expected = IllegalArgumentException.class)
//    public final void expectExceptionForMoreThan2Nos() {
//        stringAccumulator.add("1,2,3");
//    }

    /**
     * 3. Allow the add method to handle new lines between numbers (instead of commas).
     *      a. The following input is ok: "1\n2,3” (will equal 6).
     */
    @Test
    public final void allowValidNewLineBetweenNos() {
        assertEquals(6, stringAccumulator.add("1\n2,3"));
    }

    /**
     * 3. Allow the add method to handle new lines between numbers (instead of commas).
     *      b. The following input is NOT ok: "1,\n” (don’t need to prove it - just clarifying).
     */
    @Test(expected = IllegalArgumentException.class)
    public final void expectExceptionForInvalidNewLineBetweenNos() {
        assertEquals(1, stringAccumulator.add("1,\\n"));
    }

    /**
     * 4. Support different delimiters.
     *      a. To change a delimiter, the beginning of the string will contain a separate line that
     *          looks like this: "//<delimiter>\n<numbers…>”, for example "//;\n1;2” should return 3 where the delimiter is ‘;’.
     *      b. The first line is optional, all existing scenarios should still be supported.
     */
    @Test
    public final void optionalSpecificationOfDelimiters() {
        assertEquals(15, stringAccumulator.add("//;\n1,2,3,4;5"));
        assertEquals(16, stringAccumulator.add("//;\n1;,2;;,3,4,6"));
        assertEquals(21, stringAccumulator.add("1,2,3,4,5,6"));
    }

    /**
     * 5. Calling add with a negative number will throw an exception with the message "negatives not allowed” - and the negative that was passed.
     *      a. If there are multiple negatives, show all of them in the exception message.
     */
    @Test
    public final void negativeNosThrowException() {
        expectedEx.expect(IllegalArgumentException.class);
        expectedEx.expectMessage("negatives not allowed  : [-2, -3, -4]");
        assertEquals(15, stringAccumulator.add("//;\n1,-2,-3,-4,5"));
    }

    /**
     * 6. Numbers bigger than 1000 should be ignored, so adding 2 + 1001 = 2.
     */
    @Test
    public final void nosGreaterThan1000ShouldBeIgnored() {
        assertEquals(10, stringAccumulator.add("//&\n1&2000&10001,4,5"));
        assertEquals(10, stringAccumulator.add("//;|*\n1,2000,10001*4,5"));
        assertEquals(19, stringAccumulator.add("1,2000,3,4,5,6"));
    }

    /**
     * 7. Delimiters can be of any length, for example: "//***\n1***2***3” should return 6.
     */
    @Test
    public final void delimitersCanBeOfAnyLength() {
        assertEquals(6, stringAccumulator.add("//***\n1***2***3"));
    }

    /**
     * 8. Allow multiple delimiters like this: "//delim1|delim2\n” (with a "|” separating delimiters),
     *      for example "//*|%\n1*2%3” should return 6.
     */
    @Test
    public final void multipleDelimitersAllowed() {
        assertEquals(6, stringAccumulator.add("//*|%\n1*2%3"));
    }

    /**
     * 9. Make sure you can also handle multiple delimiters with length longer than one character
     */
    @Test
    public final void multipleDelimitersLongerThan1CharacterAllowed() {
        assertEquals(6, stringAccumulator.add("//**|%%\n1**2%%3"));
        assertEquals(6, stringAccumulator.add("1,,2,,3"));
        assertEquals(11, stringAccumulator.add("//**|%%|&&\n1&&5**2%%3"));
        assertEquals(10, stringAccumulator.add("//**|%%|&&\n1000&&5**2%%3"));
    }

    /**
     *  Non specified delimiter should throw exception
     */
    @Test(expected = NumberFormatException.class)
    public final void nonSpecifiedDelimitersShouldThrowException() {
        assertEquals(6, stringAccumulator.add("1;;2;3"));
    }




}